


def compute(threshold, lines):
    right = 0
    wrong = 0
    
    for line in lines:
        line_list = line.split()
        if int(line_list[0]) == 1 and float(line_list[3]) > threshold or \
        int(line_list[0]) == 0 and float(line_list[3]) <= threshold:
            right += 1
        else:
            wrong += 1
    return right/(right+wrong)
if __name__ == "__main__":
    max_prec = -1
    f = open("../scores/VoxCeleb1-Clean.foo", 'r')
    lines = f.readlines()
    for th in range(10, 100):
        prec = compute(th, lines)
        if prec >= max_prec:
            max_prec = prec
            max_th = th
    print(f"max precision is {max_prec} when threshold is {max_th}")
        
    
